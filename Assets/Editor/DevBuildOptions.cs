﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System;
using UnityEditor.SceneManagement;

namespace VGMEditorTools
{
	public class DevBuildOptions : EditorWindow
	{
		private bool toggleScenes 	= true;
		private static bool synced	= false;

		private float LastBuildSize 
		{
			get
			{
				return EditorPrefs.GetFloat ("LastBuildSize", 0);
			}
			set
			{
				EditorPrefs.SetFloat ("LastBuildSize", value);
			}
		}

		private float CurrentBuildSize 
		{
			get
			{
				return EditorPrefs.GetFloat ("CurrentBuildSize", 0);
			}
			set
			{
				EditorPrefs.SetFloat ("CurrentBuildSize", value);
			}
		}

		public string DifferenceInSize = "";

		private static bool HasStartedBuild 
		{
			get{ return EditorPrefs.GetBool ("HasStartedBuild", false);}
			set{EditorPrefs.SetBool ("HasStartedBuild", value);}
		}

		private static BuildType buildType
		{
			get{ return (BuildType) EditorPrefs.GetInt ("BuildType", 0);}
			set{EditorPrefs.SetInt ("BuildType", (int) value);}
		}

		private enum BuildType
		{
			None,
			AutoRun,
			ThenRun
		}

		private static string LastBuildFolder
		{
			get
			{
				return EditorPrefs.GetString ("LastSaveFolder");
			}
			set
			{
				EditorPrefs.SetString ("LastSaveFolder",value);
			}
		}

		public string saveFileName 
		{
			get 
			{ 
				return Application.identifier + "_" + EditorUserBuildSettings.activeBuildTarget + ".apk";
			}
		}

		public string GetProjectBuildFolder
		{
			get
			{
				return Path.Combine (Application.dataPath.Replace ("Assets", ""), "Builds");
			}
		}

		public string GetTempBuildFolder
		{
			get
			{
				return Path.Combine (Environment.GetEnvironmentVariable("temp"), "UnityTempBuilds" + Path.AltDirectorySeparatorChar + Application.identifier + Path.AltDirectorySeparatorChar + "Builds");
			}
		}

		private SavedSceneCollection SceneCollection
		{
			get
			{
				if (_SceneCollection == null)
				{
					_SceneCollection = new SavedSceneCollection (new SavedScene[]{});
				}
				return _SceneCollection;
			}
			set
			{
				_SceneCollection = value;
			}
		}

		private SavedSceneCollection _SceneCollection;

		[MenuItem("Window/Dev Build Options")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow(typeof(DevBuildOptions));
			synced = false;
		}

		[PostProcessBuildAttribute(1)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) 
		{
			Debug.Log ("Refreshing");
			synced = false;
		}

		private void Resync()
		{
			LoadData ();
			if (HasStartedBuild)
			{
				string lastPath = EditorUserBuildSettings.GetBuildLocation (EditorUserBuildSettings.activeBuildTarget);
				if (!string.IsNullOrEmpty (lastPath))
				{
					FileInfo info = new FileInfo(lastPath);
					if (info.Exists)
					{
						LastBuildSize = CurrentBuildSize;

						CurrentBuildSize =  (float) info.Length / (float) (1024 * 1024);
						CurrentBuildSize =  (float) Math.Round ((double)CurrentBuildSize, 2);

						SetDiffInSize (CurrentBuildSize, LastBuildSize);
					}
					string path = Path.Combine (Environment.GetEnvironmentVariable ("localappdata"), "Unity" + Path.AltDirectorySeparatorChar + "Editor" + Path.AltDirectorySeparatorChar + "Editor.log");
					File.Copy (path, LastBuildFolder + "Editor.Log", true);
					if (buildType == BuildType.ThenRun)
					{
						Rebuild ();
					}
				}
			}
			HasStartedBuild = false;
			synced = true;
		}

		float scrollPosY = 0f;
		private void OnGUI()
		{
			if (!synced)
			{
				Resync ();
			}
			if (GUILayout.Button ("Sync From Player"))
			{
				SceneCollection = new SavedSceneCollection(EditorBuildSettings.scenes);
				toggleScenes = true;
			}
			if (SceneCollection != null && SceneCollection.Scenes != null && SceneCollection.Scenes.Length > 0)
			{
				GUILayout.Space (5);

				GUILayout.Box ("Last build size (MB) : " + CurrentBuildSize);

				if(!string.IsNullOrEmpty(DifferenceInSize))
				GUILayout.Box("( "+ DifferenceInSize +" )");

				if (GUILayout.Button ("Build"))
				{
					BuildPlayer ();
				}
				buildType = (BuildType) EditorGUILayout.EnumPopup (buildType);
				if (GUILayout.Button ("Open Last Build Folder"))
				{
					OpenBuildFolder ();
				}
				if (GUILayout.Button (new GUIContent("Save Last Build", "Last Build will be saved from Temp folder")))
				{
					SaveLastBuild ();				
				}
				GUILayout.Space (10);
				if (GUILayout.Button ("Start Logcat"))
				{
					StartLogcat ();
				}
				if (GUILayout.Button ("Install last Build"))
				{
					Rebuild ();
				}
				if (GUILayout.Button ("Uninstall from Device"))
				{
					Uninstall ();
				}
				GUILayout.Space (10);

				string val = toggleScenes ? "Deactivate All Scenes" : "Activate All Scenes";

				EditorGUI.BeginChangeCheck ();
				if (GUILayout.Button (val))
				{
					toggleScenes = !toggleScenes;
					foreach(SavedScene scene in SceneCollection.Scenes) 
					{
						scene.enabled = toggleScenes;
					}
				}
				GUILayout.Space (5f);
				scrollPosY = GUILayout.BeginScrollView (new Vector2 (0, scrollPosY)).y;
				foreach(SavedScene scene in SceneCollection.Scenes) 
				{
					GUILayout.BeginHorizontal ();
					scene.enabled = GUILayout.Toggle (scene.enabled, GUIContent.none, GUILayout.MaxWidth(20f));
					if (GUILayout.Button (GetSimplifiedFileName (scene.path), GUILayout.MaxWidth(120f)))
					{
						EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
						EditorSceneManager.OpenScene (scene.path, OpenSceneMode.Single);
					}
					GUILayout.EndHorizontal ();
				}

				GUILayout.EndScrollView ();
				if (EditorGUI.EndChangeCheck ())
				{
					SaveData ();
				}
			}
		}

		private void SetDiffInSize(float currentVal, float lastVal)
		{
			if (currentVal == 0 || lastVal == 0)
			{
				DifferenceInSize = "";
			}
			else
			{
				int val = (int) (((currentVal / lastVal) * 100f) - 100f);
				if (val < 0)
				{
					DifferenceInSize = "Reduced by " + (Mathf.Abs (val)) + "%";
				}
				else if (val > 0)
				{
					DifferenceInSize = "Increased by " + (Mathf.Abs (val)) + "%";
				}
				else
				{
					DifferenceInSize = "No Difference";
				}
			}
		}

		private void BuildPlayer()
		{
			if (SceneCollection != null && SceneCollection.IsValid())
			{
				SaveData ();
				DateTime dt = DateTime.Now;
				LastBuildFolder = GetTempBuildFolder + Path.AltDirectorySeparatorChar + dt.ToString ("yyyy_MM_dd_HH_mm") + Path.AltDirectorySeparatorChar;
				DirectoryInfo Directory = new DirectoryInfo (LastBuildFolder);
				if (!Directory.Exists)
				{
					Directory.Create ();
				}
				string extraDataFilename = "ExtraInfo.txt";
				StreamWriter writer = File.CreateText (LastBuildFolder + extraDataFilename);

				string val = "";
				foreach (SavedScene scene in SceneCollection.Scenes)
				{
					if (scene.enabled)
					{
						val += scene.path + Environment.NewLine;
					}
				}
				writer.WriteLine (val);
				writer.Close ();
				EditorUserBuildSettings.SetBuildLocation (EditorUserBuildSettings.activeBuildTarget, LastBuildFolder + saveFileName);
				BuildOptions opt = buildType == BuildType.AutoRun ? BuildOptions.AutoRunPlayer : BuildOptions.None;
				BuildPipeline.BuildPlayer (SceneCollection.GetEditorScenes(), LastBuildFolder + saveFileName, EditorUserBuildSettings.activeBuildTarget, opt);
				HasStartedBuild = true;
			}
			else
			{
				EditorUtility.DisplayDialog ("Error", "No scenes added", "Ok");
			}
		}

		private void Uninstall()
		{
			string path = Path.Combine(Path.Combine(EditorPrefs.GetString ("AndroidSdkRoot") , "platform-tools"),"adb ");
			string para = "uninstall " + Application.identifier;
			string cmd = path + para;
			System.Diagnostics.Process.Start ("Powershell.exe", cmd);
		}

		private void StartLogcat()
		{
			string path = Path.Combine(Path.Combine(EditorPrefs.GetString ("AndroidSdkRoot") , "platform-tools"),"adb ");
			string para = "logcat -s Unity";
			string cmd = path + para;
			System.Diagnostics.Process.Start ("Powershell.exe", cmd);
		}

		private static void Rebuild()
		{
			string path = Path.Combine(Path.Combine(EditorPrefs.GetString ("AndroidSdkRoot") , "platform-tools"),"adb ");
			string para = "install " + EditorUserBuildSettings.GetBuildLocation (EditorUserBuildSettings.activeBuildTarget);
			string cmd = path + para;
			System.Diagnostics.Process.Start ("Powershell.exe", cmd);
		}

		private static void OpenBuildFolder()
		{
			string location = EditorUserBuildSettings.GetBuildLocation (EditorUserBuildSettings.activeBuildTarget);
			location = location.Replace(@"/", @"\");
			System.Diagnostics.Process.Start("explorer.exe", "/select," + location);
		}

		private void SaveLastBuild()
		{
			DirectoryInfo tempBuildFolder = new DirectoryInfo (LastBuildFolder);
			if (!tempBuildFolder.Exists)
			{
				Debug.Log (tempBuildFolder.FullName);
				Debug.Log ("Not found.. Folder Deleted or Modified");
				return;
			}
			else
			{
				DirectoryInfo projectBuildFolder = new DirectoryInfo (GetProjectBuildFolder);
				if (!projectBuildFolder.Exists)
				{
					projectBuildFolder.Create ();
				}

				DirectoryInfo subFolder = projectBuildFolder.CreateSubdirectory (tempBuildFolder.Name);			
				foreach (FileInfo file in tempBuildFolder.GetFiles())
				{
					file.CopyTo (Path.Combine (subFolder.FullName, file.Name));
				}
			}
		} 

		public static string GetSimplifiedFileName(string sceneName)
		{
			int lastDotIdx = sceneName.LastIndexOf('.');
			int lastSlashdx = sceneName.LastIndexOf ('/');
			return sceneName.Substring(lastSlashdx + 1, lastDotIdx - lastSlashdx - 1);
		}

		private void SaveData()
		{
			EditorPrefs.SetString("SavedScenes", EditorJsonUtility.ToJson(SceneCollection, true));
		}

		private void LoadData()
		{
			EditorJsonUtility.FromJsonOverwrite(EditorPrefs.GetString ("SavedScenes"), SceneCollection);
		}
	}

	[System.Serializable]
	public class SavedSceneCollection
	{
		[SerializeField]
		public SavedScene[] Scenes;

		public SavedSceneCollection(SavedScene[] scenes)
		{
			Scenes = scenes;
		}

		public SavedSceneCollection(EditorBuildSettingsScene[] scenes)
		{
			Scenes = new SavedScene[scenes.Length];
			for (int i = 0; i < scenes.Length; i++)
			{
				Scenes [i] = new SavedScene (scenes[i]);
			}
		}

		public EditorBuildSettingsScene[] GetEditorScenes()
		{
			EditorBuildSettingsScene[] editorScenes = new EditorBuildSettingsScene[Scenes.Length];
			for (int i = 0; i < Scenes.Length; i++)
			{
				editorScenes [i] = new EditorBuildSettingsScene (Scenes [i].path, Scenes [i].enabled);
			}
			return editorScenes;
		}

		public string[] GetSceneNames()
		{
			string[] val = new string[Scenes.Length];
			for (int i = 0; i < Scenes.Length; i++)
			{
				val [i] = DevBuildOptions.GetSimplifiedFileName(Scenes[i].path);
			}
			return val;
		}

		public bool IsValid()
		{
			if (Scenes != null && Scenes.Length > 0)
			{
				foreach (SavedScene scene in Scenes)
				{
					if (scene.enabled)
					{
						return true;
					}
				}
			}
			return false;
		}
	}

	[System.Serializable]
	public class SavedScene
	{
		public string path;
		public bool enabled;

		public SavedScene(string path, bool enabled)
		{
			this.path = path;
			this.enabled = enabled;
		}

		public SavedScene(EditorBuildSettingsScene scene)
		{
			this.path = scene.path;
			this.enabled = scene.enabled;
		}
	}	
}