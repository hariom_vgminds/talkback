﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.Threading.O7Thread
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Threading;

namespace Outfit7.Threading
{
  public class O7Thread
  {
    private readonly Thread Thread;

    public O7Thread(Action job)
    {
      this.Thread = new Thread(new ThreadStart(job.Invoke));
    }

    public static int CurrentThreadId
    {
      get
      {
        return Thread.CurrentThread.ManagedThreadId;
      }
    }

    public static void Sleep(int millisecondsTimeout)
    {
      Thread.Sleep(millisecondsTimeout);
    }

    public static void Sleep(TimeSpan timeout)
    {
      long totalMilliseconds = (long) timeout.TotalMilliseconds;
      if (totalMilliseconds < -1L || totalMilliseconds > (long) int.MaxValue)
        throw new ArgumentOutOfRangeException();
      Thread.Sleep((int) timeout.TotalMilliseconds);
    }

    public string Name
    {
      get
      {
        return this.Thread.Name;
      }
      set
      {
        this.Thread.Name = value;
      }
    }

    public bool IsBackground
    {
      get
      {
        return this.Thread.IsBackground;
      }
      set
      {
        this.Thread.IsBackground = value;
      }
    }

    public bool IsAlive
    {
      get
      {
        return this.Thread.IsAlive;
      }
    }

    public void Start()
    {
      this.Thread.Start();
    }

    public void Join()
    {
      this.Join(-1);
    }

    public void Join(int millisecondsTimeout)
    {
      this.Thread.Join(millisecondsTimeout);
    }

    public void Join(TimeSpan timeout)
    {
      long totalMilliseconds = (long) timeout.TotalMilliseconds;
      if (totalMilliseconds < -1L || totalMilliseconds > (long) int.MaxValue)
        throw new ArgumentOutOfRangeException();
      this.Join((int) timeout.TotalMilliseconds);
    }
  }
}
