﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.TalkBack.RecordingBuffer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using Outfit7.Threading;
using System;
using System.Threading;
using UnityEngine;

namespace Outfit7.TalkBack
{
  public class RecordingBuffer
  {
    private bool FirstRun = true;
    private readonly object AddSamplesLock = new object();
    private readonly object SamplesLock = new object();
    public const string Tag = "RecordingBuffer";
    public ProcessedSound ProcessedSound;
    public volatile bool Running;
    private volatile bool DoneCollectingData;
    private volatile int SamplesCaptured;
    private float AudioBufferAvgSampleValue;
    private readonly TalkBackSettings TalkBackSettings;
    private int DataAnalyseIndex;
    private readonly int BatchSize;
    private float[] Samples;
    private float AudioBufferMaxSampleValue;
    private readonly int MinSilenceSamples;
    private readonly bool[] SamplesChunkSilence;
    private int SamplesChunkIndex;
    private readonly int MaxSamplesToCapture;
    private readonly int SampleRate;
    private readonly float[] AudioBufferLastFramesMax;
    private int AudioBufferSize;
    private O7Thread SoundConversionThread;

    public RecordingBuffer(TalkBackSettings talkBackSettings)
    {
      this.TalkBackSettings = talkBackSettings;
      this.SampleRate = this.TalkBackSettings.SampleRate;
      this.MaxSamplesToCapture = this.SampleRate * this.TalkBackSettings.MaxRecordingTime;
      this.AudioBufferLastFramesMax = new float[this.TalkBackSettings.ListenAverageFramesCount];
      this.BatchSize = Mathf.CeilToInt((float) this.SampleRate / (float) this.TalkBackSettings.TalkFramesPerSecond);
      this.MinSilenceSamples = Mathf.RoundToInt(talkBackSettings.MicrophoneRecordingStartDelay * (float) this.SampleRate);
      this.Samples = new float[this.MaxSamplesToCapture];
      this.ProcessedSound = new ProcessedSound(talkBackSettings);
      this.SamplesChunkSilence = new bool[this.MaxSamplesToCapture / this.MinSilenceSamples];
    }

    public void ClearBuffer()
    {
      Array.Clear((Array) this.Samples, 0, this.Samples.Length);
      Array.Clear((Array) this.SamplesChunkSilence, 0, this.SamplesChunkSilence.Length);
      this.ProcessedSound.ResetAndClear();
    }

    public void SetDoneCollectingData()
    {
      this.DoneCollectingData = true;
    }

    public bool EnoughtSamplesForSilenceDetection
    {
      get
      {
        return this.SamplesCaptured > this.MinSilenceSamples;
      }
    }

    public void Abort()
    {
      this.Running = false;
      if (this.SoundConversionThread != null)
      {
        lock (this.AddSamplesLock)
        {
          this.Running = false;
          Monitor.Pulse(this.AddSamplesLock);
        }
        this.SoundConversionThread.Join();
      }
      this.SoundConversionThread = (O7Thread) null;
    }

    public void StartConverting()
    {
      if (this.SoundConversionThread != null)
      {
        lock (this.AddSamplesLock)
        {
          this.Running = false;
          Monitor.Pulse(this.AddSamplesLock);
        }
        this.SoundConversionThread.Join();
      }
      this.SoundConversionThread = new O7Thread(new Action(this.ProcessSamples));
      this.SoundConversionThread.Name = "SoundConversionAndAnalysis";
      this.Running = true;
      this.SoundConversionThread.Start();
    }

    public void InitSamplesBuffer()
    {
      this.ProcessedSound.ResetAndClear();
      this.SamplesChunkIndex = 0;
      this.SamplesCaptured = 0;
      this.AudioBufferAvgSampleValue = 0.0f;
      this.DoneCollectingData = false;
    }

    private void CalcNewSilenceAvg()
    {
      float num = 0.0f;
      for (int index = 0; index < this.AudioBufferSize; ++index)
        num += this.AudioBufferLastFramesMax[index];
      this.AudioBufferAvgSampleValue = num / (float) this.AudioBufferSize;
    }

    private void AddNewSilenceFrameMax(float value)
    {
      ++this.AudioBufferSize;
      if (this.AudioBufferSize > this.TalkBackSettings.ListenAverageFramesCount)
        this.AudioBufferSize = this.TalkBackSettings.ListenAverageFramesCount;
      for (int index = 1; index < this.TalkBackSettings.ListenAverageFramesCount; ++index)
        this.AudioBufferLastFramesMax[index - 1] = this.AudioBufferLastFramesMax[index];
      this.AudioBufferLastFramesMax[this.AudioBufferLastFramesMax.Length - 1] = value;
    }

    public void CalculateSilenceBarrier()
    {
      float num = 0.0f;
      for (int index = 0; index < this.MinSilenceSamples; ++index)
      {
        if ((double) num < (double) this.Samples[index])
          num = Mathf.Abs(this.Samples[index]);
      }
      if ((double) num > 1.0)
        num = 1f;
      this.AddNewSilenceFrameMax(num);
      this.CalcNewSilenceAvg();
      this.SamplesCaptured = 0;
    }

    public bool ShouldStartRecording()
    {
      float num1 = this.AudioBufferAvgSampleValue * this.TalkBackSettings.ListenSilenceStartFactor;
      if ((double) num1 < (double) this.AudioBufferAvgSampleValue + (double) this.TalkBackSettings.ListenSilenceEnviromentAbsDeltaStart)
        num1 = this.AudioBufferAvgSampleValue + this.TalkBackSettings.ListenSilenceEnviromentAbsDeltaStart;
      float num2 = 0.0f;
      int num3 = 0;
      for (int index = 0; index < this.MinSilenceSamples; ++index)
      {
        float num4 = Mathf.Abs(this.Samples[index]);
        num2 += num4;
        if ((double) num4 > (double) num1)
          ++num3;
      }
      float num5 = (float) this.MinSilenceSamples * this.TalkBackSettings.ListenStartRecordingFactor;
      if ((double) num2 >= (double) num5)
        return num3 > 20;
      return false;
    }

    public bool ShouldStopRecording()
    {
      int offset = this.SamplesChunkIndex * this.MinSilenceSamples;
      if (this.SamplesCaptured > offset + this.MinSilenceSamples)
      {
        this.SamplesChunkSilence[this.SamplesChunkIndex] = this.IsSilence(offset);
        ++this.SamplesChunkIndex;
      }
      int num = 0;
      for (int index = this.SamplesChunkIndex - 1; index > 0 && this.SamplesChunkSilence[index]; --index)
        ++num;
      return num >= this.TalkBackSettings.ListenStopMaxSilenceChunks;
    }

    private bool IsSilence(int offset)
    {
      float num1 = this.AudioBufferAvgSampleValue * this.TalkBackSettings.ListenSilenceEndFactor;
      if ((double) num1 < (double) this.AudioBufferAvgSampleValue + (double) this.TalkBackSettings.ListenSilenceEnviromentAbsDeltaEnd)
        num1 = this.AudioBufferAvgSampleValue + this.TalkBackSettings.ListenSilenceEnviromentAbsDeltaEnd;
      int num2 = 0;
      for (int index = 0; index < this.MinSilenceSamples; ++index)
      {
        if ((double) Mathf.Abs(this.Samples[index + offset]) > (double) num1)
          ++num2;
      }
      return num2 < 20;
    }

    public int AddSamples(float[] data, int numOfSamples)
    {
      int length = numOfSamples / this.TalkBackSettings.MicrophoneChannels;
      if (this.SamplesCaptured + length >= this.MaxSamplesToCapture)
        length = this.MaxSamplesToCapture - this.SamplesCaptured - 1;
      if (length > 0)
      {
        if (this.TalkBackSettings.MicrophoneChannels == 1)
        {
          lock (this.SamplesLock)
            Array.Copy((Array) data, 0, (Array) this.Samples, this.SamplesCaptured, length);
        }
        else
        {
          lock (this.SamplesLock)
          {
            for (int index = 0; index < length; ++index)
              this.Samples[index + this.SamplesCaptured] = data[index * this.TalkBackSettings.MicrophoneChannels];
          }
        }
        this.SamplesCaptured += length;
      }
      return length;
    }

    public void Notify()
    {
      lock (this.AddSamplesLock)
        Monitor.Pulse(this.AddSamplesLock);
    }

    private void ProcessSamples()
    {
      try
      {
        this.AudioBufferMaxSampleValue = 0.0f;
        this.DataAnalyseIndex = 0;
        int offsetIn = 0;
        SoundTouchPlugin.InitSoundTouch(this.TalkBackSettings.SoundTouchTempo, this.TalkBackSettings.SoundTouchPitch, this.TalkBackSettings.SoundTouchRate, this.SampleRate);
        while (this.Running)
        {
          int samplesCaptured = this.SamplesCaptured;
          if (offsetIn < samplesCaptured)
          {
            int sizeIn = samplesCaptured - offsetIn;
            if (!this.DoneCollectingData && sizeIn > this.TalkBackSettings.SoundTouchMaxSamplesToProcess)
              sizeIn = this.TalkBackSettings.SoundTouchMaxSamplesToProcess;
            int num1 = sizeIn;
            int sizeOut = this.ProcessedSound.Data.Length - this.ProcessedSound.Length;
            int num2 = offsetIn;
            lock (this.SamplesLock)
              SoundTouchPlugin.ProcessSound(this.Samples, offsetIn, sizeIn, this.ProcessedSound.Data, this.ProcessedSound.Length, sizeOut, out sizeOut, this.DoneCollectingData);
            offsetIn += num1;
            this.ProcessedSound.Length += sizeOut;
            for (int index = num2; index < offsetIn; ++index)
            {
              float num3 = Mathf.Abs(this.ProcessedSound.Data[index]);
              if ((double) num3 > (double) this.AudioBufferMaxSampleValue)
                this.AudioBufferMaxSampleValue = num3;
            }
          }
          this.AnalyseFrames(offsetIn < samplesCaptured);
          if (this.DoneCollectingData && samplesCaptured == offsetIn && this.DataAnalyseIndex == this.ProcessedSound.Length)
          {
            if (this.FirstRun && (double) this.AudioBufferMaxSampleValue < 0.5)
              this.AudioBufferMaxSampleValue = this.TalkBackSettings.ListenNormalizationFactor;
            this.FirstRun = false;
            this.ProcessedSound.Length = Math.Max(this.ProcessedSound.Length - (int) this.TalkBackSettings.ListenEndCutoff * this.MinSilenceSamples, 1);
            int num1 = (int) this.TalkBackSettings.ListenEndFadeout * this.MinSilenceSamples;
            float normFactor = this.GetNormFactor(this.AudioBufferMaxSampleValue);
            for (int index = 0; index < this.ProcessedSound.Length; ++index)
            {
              this.ProcessedSound.Data[index] = this.GetSampleValueInSoundNormRange(this.ProcessedSound.Data[index] * normFactor);
              int num2 = this.ProcessedSound.Length - num1;
              if (index > num2)
              {
                float num3 = (float) (1.0 - (double) (index - num2) / (double) num1);
                this.ProcessedSound.Data[index] *= num3;
              }
            }
            this.Running = false;
          }
          lock (this.AddSamplesLock)
          {
            if (this.Running)
              Monitor.Wait(this.AddSamplesLock);
          }
        }
      }
      catch (Exception ex)
      {
		 Debug.LogException(ex);	
      }
      finally
      {
				
      }
    }

    private float GetSampleValueInSoundNormRange(float val)
    {
      if ((double) val > (double) this.TalkBackSettings.ListenNormalizationFactor)
        val = this.TalkBackSettings.ListenNormalizationFactor;
      else if ((double) val < -(double) this.TalkBackSettings.ListenNormalizationFactor)
        val = -this.TalkBackSettings.ListenNormalizationFactor;
      return val;
    }

    private float GetNormFactor(float maxVal)
    {
      if ((double) maxVal != 0.0)
        return this.TalkBackSettings.ListenNormalizationFactor / maxVal;
      return this.TalkBackSettings.ListenNormalizationFactor;
    }

    private void AnalyseFrames(bool doPartial)
    {
      while (this.DataAnalyseIndex < this.ProcessedSound.Length)
      {
        int talkFrameBins = this.TalkBackSettings.TalkFrameBins;
        int num1;
        if (this.DataAnalyseIndex + this.BatchSize > this.ProcessedSound.Length)
        {
          if (!this.DoneCollectingData)
            break;
          num1 = this.ProcessedSound.Length - this.DataAnalyseIndex;
          if (num1 < this.BatchSize && doPartial)
            break;
        }
        else
          num1 = this.BatchSize;
        float num2 = 1f / (float) talkFrameBins;
        int[] numArray = new int[talkFrameBins];
        for (int index = 0; index < talkFrameBins; ++index)
          numArray[index] = 0;
        float num3 = 0.0f;
        for (int index1 = 0; index1 < num1; ++index1)
        {
          int index2 = Mathf.RoundToInt(Mathf.Abs(this.ProcessedSound.Data[index1 + this.DataAnalyseIndex]) / num2);
          if (index2 >= talkFrameBins || index2 < 0)
            index2 = talkFrameBins - 1;
          ++numArray[index2];
        }
        for (int index = 0; index < talkFrameBins; ++index)
        {
          if (numArray[index] > num1 / 50)
            num3 = (float) index;
        }
        float num4 = num3 * num2;
        float listenNoiseLimit = this.TalkBackSettings.ListenNoiseLimit;
        float f;
        if ((double) num4 < (double) listenNoiseLimit)
        {
          f = 0.0f;
        }
        else
        {
          float num5 = (float) (((double) num4 - (double) listenNoiseLimit) / (1.0 - (double) listenNoiseLimit));
          f = (double) num5 <= 0.800000011920929 ? num5 / 0.8f : 0.8f;
        }
        float num6 = Mathf.Pow(f, 1.5f);
        this.DataAnalyseIndex += num1;
        if (this.ProcessedSound.TalkFramesLength < this.ProcessedSound.TalkFrames.Length)
        {
          this.ProcessedSound.TalkFrames[this.ProcessedSound.TalkFramesLength] = num6;
          ++this.ProcessedSound.TalkFramesLength;
        }
      }
    }
  }
}
