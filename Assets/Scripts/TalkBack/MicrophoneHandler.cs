﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.TalkBack.MicrophoneHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Outfit7.TalkBack
{
  public class MicrophoneHandler : MonoBehaviour
  {
    public const string Tag = "MicrophoneHandler";
    private const int DefaultDataBufferSize = 2000;
    public TalkBackSettings TalkBackSettings;
    public Action OnRecordingStarted;
    public Action<ProcessedSound> OnConvertingDone;
    private AudioClip RecordedClip;
    private RecordingBuffer RecordingBuffer;
    private float RecordingStartTime;
    private MicrophoneHandler.MicState microphoneState;
    private float[] DataBuffer;

	public string micName = string.Empty; 

    private bool Active
    {
      get
      {
        return (this.MicrophoneState == MicrophoneHandler.MicState.Idle ? 1 : (this.MicrophoneState == MicrophoneHandler.MicState.Processed ? 1 : 0)) == 0;
      }
    }

	void Start()
	{
		Init ();
	}

    public void Init()
    {
      this.RecordingBuffer = new RecordingBuffer(this.TalkBackSettings);
    }

    public MicrophoneHandler.MicState MicrophoneState
    {
      get
      {
        return this.microphoneState;
      }
      private set
      {
        this.microphoneState = value;
        switch (this.microphoneState)
        {
          case MicrophoneHandler.MicState.Recording:
            if (this.OnRecordingStarted == null)
              break;
            this.OnRecordingStarted();
            break;
          case MicrophoneHandler.MicState.Processing:
            this.RecordingBuffer.SetDoneCollectingData();
            break;
          case MicrophoneHandler.MicState.Processed:
            if (this.OnConvertingDone == null)
              break;
            this.OnConvertingDone(this.RecordingBuffer.ProcessedSound);
            break;
        }
      }
    }

    public void Stop()
    {
      this.RecordingBuffer.Abort();
      this.RecordingBuffer.ClearBuffer();
      this.ReleaseMicrophone();
    }

    public void Reset()
    {
      if (this.RecordingBuffer == null)
        return;
      this.RecordingBuffer.ClearBuffer();
    }

    public void AcquireMicrophone()
    {
      if (this.Active)
        return;
      this.ReleaseMicrophone();
      this.RecordingStartTime = Time.time;
      if (Microphone.devices.Length == 0)
        return;

	  StartHeadphone ();
      this.RecordingBuffer.InitSamplesBuffer();
	  this.MicrophoneState = MicrophoneHandler.MicState.AquiringMicrophone;
    }

    public void ReleaseMicrophone()
    {
	  this.MicrophoneState = MicrophoneHandler.MicState.Idle;
	  StopHeadphone ();
      if (this.RecordingBuffer == null)
        return;
      this.RecordingBuffer.Abort();
    }

    private bool IsMicrophoneAcquired()
    {
		return IsHeadphoneStarted ();
    }

    private void Update()
    {
      switch (this.MicrophoneState)
      {
        case MicrophoneHandler.MicState.Idle:
          break;
        case MicrophoneHandler.MicState.AquiringMicrophone:
          if (!this.IsMicrophoneAcquired())
            break;
          this.MicrophoneState = MicrophoneHandler.MicState.AquiringSilenceBarrier;
          break;
        case MicrophoneHandler.MicState.AquiringSilenceBarrier:
          this.AddSamplesToRecordingBuffer();
          if (!this.RecordingBuffer.EnoughtSamplesForSilenceDetection)
            break;
          this.RecordingBuffer.CalculateSilenceBarrier();
          this.MicrophoneState = MicrophoneHandler.MicState.AnalysingSound;
          break;
        case MicrophoneHandler.MicState.AnalysingSound:
          this.AddSamplesToRecordingBuffer();
          if (!this.RecordingBuffer.EnoughtSamplesForSilenceDetection)
            break;
          if (this.RecordingBuffer.ShouldStartRecording())
          {
            this.MicrophoneState = MicrophoneHandler.MicState.Recording;
            this.RecordingBuffer.StartConverting();
            break;
          }
          this.RecordingBuffer.CalculateSilenceBarrier();
          break;
        case MicrophoneHandler.MicState.Recording:
          int recordingBuffer = this.AddSamplesToRecordingBuffer();
          if (recordingBuffer < 0 || recordingBuffer != 0 && !this.RecordingBuffer.ShouldStopRecording())
            break;
          this.MicrophoneState = MicrophoneHandler.MicState.Processing;
          break;
        case MicrophoneHandler.MicState.Processing:
          this.RecordingBuffer.Notify();
          if (this.RecordingBuffer.Running)
            break;
          this.MicrophoneState = MicrophoneHandler.MicState.Processed;
          break;
        case MicrophoneHandler.MicState.Processed:
          this.microphoneState = MicrophoneHandler.MicState.Idle;
          break;
        default:
          throw new InvalidProgramException("Unhandled microphone state.");
      }
    }

    private int AddSamplesToRecordingBuffer()
    {
      try
      {
        if ((double) this.RecordingStartTime + (double) this.TalkBackSettings.MicrophoneRecordingStartDelay > (double) Time.time)
          return -1;
        int numOfSamples = 0;
		float[] array = new float[DefaultDataBufferSize];
		RecordedClip.GetData(array,0);
        if (array != null)
          numOfSamples = array.Length;
        if (array == null || array.Length == 0)
          return -1;
        return this.RecordingBuffer.AddSamples(array, numOfSamples);
      }
      finally
      {
        this.RecordingBuffer.Notify();
      }
    }

    private void OnDestroy()
    {
      this.ReleaseMicrophone();
    }

    private void OnApplicationPause(bool paused)
    {
      if (paused)
        this.Stop();
      else
        this.Reset();
    }

    public enum MicState
    {
      Idle,
      AquiringMicrophone,
      AquiringSilenceBarrier,
      AnalysingSound,
      Recording,
      Processing,
      Processed,
    }

	public void StartHeadphone()
	{
		RecordedClip = Microphone.Start (null, true, 1, 44100);
		GetComponent<AudioSource> ().clip = RecordedClip;
		GetComponent<AudioSource> ().Play ();
	}

	public void StopHeadphone()
	{
		GetComponent<AudioSource> ().clip = null;
		GetComponent<AudioSource> ().Stop ();
		Microphone.End (null);
	}

	public bool IsHeadphoneStarted()
	{
		return Microphone.IsRecording (null);
	}
  }
}
