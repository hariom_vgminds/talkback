﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Outfit7.TalkBack;
using System;

public class TalkbackController : MonoBehaviour 
{
	public TalkBackHandler TalkBackHandler;
	public AudioSource AudioSource;

	public string currentTask
	{
		get
		{
			return _currentTask;
		}
		set
		{
			if (_currentTask == value)
			{
				return;
			}
			Debug.Log ("Changing from " + _currentTask + " to " + value);
			_currentTask = value;
		}
	}

	public string _currentTask;

	public void Start()
	{
		
	}

	public void ListenEvent()
	{
		currentTask = "Listening";
//		this.currentTalkFrame = -1f;
//		this.AnimationController.fireAction(AnimationAction.Listen);
	}

	public void TalkEvent(float talkFrame)
	{
		currentTask = "Talking";
//		this.nextTalkFrame = talkFrame;
//		if ((double) this.nextTalkFrame == (double) this.currentTalkFrame)
//			return;
//		this.AnimationController.fireAction(AnimationAction.Talk);
	}

	public void DoneTalkingEvent(bool interrupted)
	{
		currentTask = "Talking Finished";
//		Challenges.TomRepeats.Increment();
//		this.currentTalkFrame = -1f;
//		if (!interrupted)
//			this.AnimationController.fireAction(AnimationAction.Mood);
//		Main.Instance.AchievementManager.IncrementStepsAndSend(Main.Instance.AchievementManager.CopyCat);
//		Main.Instance.AchievementManager.IncrementStepsAndSend(Main.Instance.AchievementManager.Chatterbox);
	}

	public void ListenStopEvent(float length)
	{
		currentTask = "Listening Finished";
//		this.AnimationController.fireAction(AnimationAction.ListenStop);
//		if (!this.AnimationController.TalkBackHandler.CanTalk)
//			return;
		this.TalkBackHandler.StartTalking();
	}


	public void EnableRepeating(bool enable)
	{
		if (enable)
		{
			this.TalkBackHandler.CallbackTalk = new Action<float>(TalkEvent);
			this.TalkBackHandler.CallbackRecordingStarted = new Action(ListenEvent);
			this.TalkBackHandler.CallbackRecordingStopped = new Action<float>(ListenStopEvent);
			this.TalkBackHandler.CallbackTalkingStopped = new Action<bool>(DoneTalkingEvent);
//			if (!this.CanListen())
//				return;
			this.StartListening();
		}
		else
		{
			this.TalkBackHandler.CallbackTalk = (Action<float>) null;
			this.TalkBackHandler.CallbackRecordingStarted = (Action) null;
			this.TalkBackHandler.CallbackRecordingStopped = (Action<float>) null;
			this.TalkBackHandler.CallbackTalkingStopped = (Action<bool>) null;
			this.StopListening();
		}
	}

	private void StopListening()
	{
		this.AudioSource.Stop();
		this.TalkBackHandler.StopListening();
	}

	private void StartListening()
	{
		this.AudioSource.Stop();
		this.TalkBackHandler.TryStartListening();
	}

	private void OnGUI()
	{
		GUILayout.BeginArea (new Rect (200, 400, 600, 600));
		GUILayout.BeginVertical ();

		GUILayout.Box (currentTask, GUILayout.MaxHeight (100), GUILayout.MaxWidth (200));

		if (GUILayout.Button ("Acquire Mic", GUILayout.MaxHeight(100), GUILayout.MaxWidth(200)))
		{
			EnableRepeating (true);
		}
		if (GUILayout.Button ("Release Mic", GUILayout.MaxHeight(200), GUILayout.MaxWidth(200)))
		{
			EnableRepeating (false);
		}
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
	}
}
