﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.TalkBack.TalkBackHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Outfit7.TalkBack
{
  public class TalkBackHandler : MonoBehaviour
  {
    public List<TalkBackHandler.TalkFrame> TalkFrames = new List<TalkBackHandler.TalkFrame>(50);
    private const string Tag = "TalkBackHandler";
    public TalkBackSettings TalkBackSettings;
    public MicrophoneHandler MicrophoneHandler;
    public AudioMixerGroup TalkBackMixerGroup;
    public Action CallbackRecordingStarted;
    public Action<float> CallbackRecordingStopped;
    public Action<bool> CallbackTalkingStopped;
    public Action<float> CallbackTalk;
    private ProcessedSound ProcessedSound;
    private bool Playing;

    private AudioSource AudioSource { get; set; }

    public bool Listening { get; private set; }

    public bool CanTalk { get; private set; }

    public bool Talking
    {
      get
      {
        return this.AudioSource.isPlaying;
      }
    }

    public float Length
    {
      get
      {
        if ((UnityEngine.Object) this.AudioSource.clip != (UnityEngine.Object) null)
          return this.AudioSource.clip.length;
        return 0.0f;
      }
    }

    public float TalkPosition
    {
      get
      {
        if (!this.Talking)
          return 0.0f;
        return this.ProcessedSound.TalkFrame((float) this.AudioSource.timeSamples);
      }
    }

    public bool Mute
    {
      get
      {
        return this.AudioSource.mute;
      }
      set
      {
        this.AudioSource.mute = value;
      }
    }

    public void OnApplicationPause(bool paused)
    {
      if (!paused)
        return;
      this.Stop();
    }
		void Start()
		{
			Init ();
		}


    public void Init()
    {
      this.AudioSource = GetComponent<AudioSource>();
	  this.AudioSource.clip = null;
      this.AudioSource.Stop ();
      this.AudioSource.playOnAwake = false;
      this.AudioSource.outputAudioMixerGroup = this.TalkBackMixerGroup;
      this.ProcessedSound = new ProcessedSound(this.TalkBackSettings);
      this.MicrophoneHandler.OnRecordingStarted = new Action(this.RecordingStarted);
      this.MicrophoneHandler.Init();
    }

    private void OnDisable()
    {
      this.StopListening();
      this.StopRepeating(true);
    }

    public void Update()
    {
      if (!this.Playing)
        return;
      if (this.AudioSource.isPlaying)
        this.UpdateTalkFrame();
      else
        this.TalkingStopped();
    }

    public void Stop()
    {
      this.StopListening();
      this.StopRepeating(false);
    }

    public TalkBackHandler.TalkFrame GetTalkFrame(float offset)
    {
      float num = (!this.Talking ? 0.0f : (float) this.AudioSource.timeSamples / (float) this.ProcessedSound.SampleRate) + offset;
      for (int index = 0; index < this.TalkFrames.Count; ++index)
      {
        TalkBackHandler.TalkFrame talkFrame = this.TalkFrames[index];
        if ((double) num >= (double) talkFrame.StartTime && (double) num < (double) talkFrame.EndTime)
          return talkFrame;
      }
      return new TalkBackHandler.TalkFrame();
    }

    private float CalculateZeroCrossingFrequency(int sampleRate, float[] audioData, int start, int end)
    {
      int num1 = end - start;
      int num2 = 0;
      for (int index = start; index < end - 1; ++index)
      {
        if ((double) audioData[index] > 0.0 && (double) audioData[index + 1] <= 0.0 || (double) audioData[index] < 0.0 && (double) audioData[index + 1] >= 0.0)
          ++num2;
      }
      float num3 = (float) num1 / (float) sampleRate;
      return (float) (num2 / 2) / num3;
    }

    private void BuildTalkFrames()
    {
      this.TalkFrames.Clear();
      float num1 = -1f;
      int num2 = 0;
      float b = -1f;
      for (int index = 0; index < this.ProcessedSound.TalkFramesLength - 1; ++index)
      {
        float talkFrame = this.ProcessedSound.TalkFrames[index];
        float crossingFrequency = this.CalculateZeroCrossingFrequency(this.ProcessedSound.SampleRate, this.ProcessedSound.Data, index * (this.ProcessedSound.SampleRate / this.ProcessedSound.TalkFramesPerSecond), (index + 1) * (this.ProcessedSound.SampleRate / this.ProcessedSound.TalkFramesPerSecond));
        b = Mathf.Max(talkFrame, b);
        if ((double) num1 > 0.0 && (double) b > (double) this.TalkBackSettings.MinTalkFrameVolume && index - num2 > 5 && ((double) Mathf.Abs(num1 - crossingFrequency) > (double) this.TalkBackSettings.TalkFrameFrequencyThreshold || index == this.ProcessedSound.TalkFramesLength - 2))
        {
          this.TalkFrames.Add(new TalkBackHandler.TalkFrame()
          {
            Frequency = crossingFrequency,
            Volume = b,
            StartTime = (float) num2 / (float) this.ProcessedSound.TalkFramesPerSecond,
            EndTime = (float) index / (float) this.ProcessedSound.TalkFramesPerSecond
          });
          num2 = index;
          b = -1f;
        }
        num1 = crossingFrequency;
      }
    }

    private void OnConvertingDone(ProcessedSound processedSound)
    {
      this.Listening = false;
      this.CanTalk = true;
	  this.ProcessedSound = new ProcessedSound(TalkBackSettings);
	  this.ProcessedSound.Data = processedSound.Data;
	  this.ProcessedSound.Length = processedSound.Length;
	  this.ProcessedSound.Channels = processedSound.Channels;
	  this.ProcessedSound.SampleRate = processedSound.SampleRate;

	  AudioClip audioClip = AudioClip.Create("Recorded sample", processedSound.Length, processedSound.Channels, processedSound.SampleRate, false);

      audioClip.SetData(processedSound.Data, 0);
      this.AudioSource.clip = audioClip;
      if (this.CallbackRecordingStopped != null)
        this.CallbackRecordingStopped(audioClip.length);
      this.BuildTalkFrames();
    }

    public void TryStartListening()
    {
      if (this.MicrophoneHandler.MicrophoneState != MicrophoneHandler.MicState.Idle)
        return;
      this.MicrophoneHandler.AcquireMicrophone();
	  this.MicrophoneHandler.OnConvertingDone = new Action<ProcessedSound>(this.OnConvertingDone);
      this.CanTalk = false;
    }

    public void StopListening()
    {
      this.Listening = false;
      this.MicrophoneHandler.ReleaseMicrophone();
      this.MicrophoneHandler.OnConvertingDone = null;
    }

    public void StopRepeating(bool interrupted)
    {
      if ((UnityEngine.Object) this.AudioSource != (UnityEngine.Object) null)
        this.AudioSource.Stop();
      this.CanTalk = false;
      if (!this.Playing)
        return;
      this.TalkingStopped(interrupted);
    }

    private void PlayRecordedSound()
    {
      this.Listening = false;
      this.CanTalk = false;
      this.AudioSource.Stop();
      this.AudioSource.volume = 1f;
      this.AudioSource.loop = false;
      this.AudioSource.Play();
      this.Playing = true;
      this.UpdateTalkFrame();
    }

    public void StartTalking()
    {
      if (!this.CanTalk)
        return;
      this.CanTalk = false;
      this.PlayRecordedSound();
    }

    private void UpdateTalkFrame()
    {
      float num = this.ProcessedSound.TalkFrame((float) this.AudioSource.timeSamples);
      if (this.CallbackTalk == null)
        return;
      this.CallbackTalk(num);
    }

    private void RecordingStarted()
    {
      this.Listening = true;
      if (this.CallbackRecordingStarted == null)
        return;
      this.CallbackRecordingStarted();
    }

    private void TalkingStopped()
    {
      this.TalkingStopped(false);
    }

    private void TalkingStopped(bool interruped)
    {
      this.Playing = false;
      if (this.CallbackTalkingStopped == null)
        return;
      this.CallbackTalkingStopped(interruped);
    }

    public struct TalkFrame
    {
      public float Frequency;
      public float Volume;
      public float StartTime;
      public float EndTime;
    }


  }
}
