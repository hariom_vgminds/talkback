﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.TalkBack.ProcessedSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Outfit7.TalkBack
{
  public class ProcessedSound
  {
    public float[] Data;
    public int Length;
    public float[] TalkFrames;
    public int TalkFramesLength;
    public  int Channels;
    public  int SampleRate;
    public  int TalkFramesPerSecond;

    public ProcessedSound(TalkBackSettings listenAndRepeatSettings)
    {
      int length1 = listenAndRepeatSettings.SampleRate * listenAndRepeatSettings.MaxRecordingTime * 2;
      int length2 = Mathf.RoundToInt((float) (listenAndRepeatSettings.MaxRecordingTime * listenAndRepeatSettings.TalkFramesPerSecond)) + 1;
      this.Data = new float[length1];
      this.TalkFrames = new float[length2];
      this.SampleRate = listenAndRepeatSettings.SampleRate;
      this.TalkFramesPerSecond = listenAndRepeatSettings.TalkFramesPerSecond;
      this.Channels = listenAndRepeatSettings.MicrophoneChannels;
    }

    public void CopyTo(ProcessedSound processedSound)
    {
      if (processedSound.Channels != this.Channels)
        throw new InvalidOperationException("Can't copy!");
      if (processedSound.SampleRate != this.SampleRate)
        throw new InvalidOperationException("Can't copy!");
      if (processedSound.TalkFramesPerSecond != this.TalkFramesPerSecond)
        throw new InvalidOperationException("Can't copy!");
      if (processedSound.Data.Length != this.Data.Length)
        throw new InvalidOperationException("Can't copy!");
      if (processedSound.TalkFrames.Length != this.TalkFrames.Length)
        throw new InvalidOperationException("Can't copy!");
      processedSound.Length = this.Length;
      processedSound.TalkFramesLength = this.TalkFramesLength;
      Array.Copy((Array) this.Data, (Array) processedSound.Data, this.Data.Length);
      Array.Copy((Array) this.TalkFrames, (Array) processedSound.TalkFrames, this.TalkFrames.Length);
    }

    public void ResetAndClear()
    {
      Array.Clear((Array) this.TalkFrames, 0, this.TalkFrames.Length);
      Array.Clear((Array) this.Data, 0, this.Data.Length);
      this.TalkFramesLength = 0;
      this.Length = 0;
    }

    public float TalkFrame(float time)
    {
      int index = Mathf.RoundToInt(time / (float) (this.SampleRate / this.TalkFramesPerSecond));
      if (index >= this.TalkFrames.Length)
        index = this.TalkFrames.Length - 1;
      return this.TalkFrames[index];
    }
  }
}
