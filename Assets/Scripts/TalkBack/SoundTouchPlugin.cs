﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.TalkBack.SoundTouchPlugin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Runtime.InteropServices;

namespace Outfit7.TalkBack
{
  public static class SoundTouchPlugin
  {
    private const string DllName = "SoundTouch";

    [DllImport("SoundTouchPlugin")]
    private static extern void _InitSoundTouch(float tempo, float pitch, float rate, int sampleRate);

    [DllImport("SoundTouchPlugin")]
    private static extern bool _ProcessSound([In] float[] array, int offset, ref int size, [Out] float[] arrayOut, int offsetOut, ref int sizeOut, bool flush);

    public static void InitSoundTouch(float tempo, float pitch, float rate, int sampleRate)
    {
//      SoundTouchPlugin._InitSoundTouch(tempo, pitch, rate, sampleRate);
    }

    public static bool ProcessSound(float[] audioIn, int offsetIn, int sizeIn, float[] audioOut, int offsetOut, int maxSizeOut, out int sizeOut, bool flush)
    {
      sizeOut = maxSizeOut;

			return true;

//      return SoundTouchPlugin._ProcessSound(audioIn, offsetIn, ref sizeIn, audioOut, offsetOut, ref sizeOut, flush);
    }
  }
}
