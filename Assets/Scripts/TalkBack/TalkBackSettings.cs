﻿// Decompiled with JetBrains decompiler
// Type: Outfit7.TalkBack.TalkBackSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 05EF8537-19E8-4CB3-AF20-5AE4DFB9B8BD
// Assembly location: C:\Users\User\Downloads\Apk\My Talking Tom_v4.3.1.7_apkpure.com\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Outfit7.TalkBack
{
  public class TalkBackSettings : MonoBehaviour
  {
    public int MinRecordingTime = 5;
    public int MaxRecordingTime = 15;
    public int TalkFramesPerSecond = 15;
    public float TalkFrameFrequencyThreshold = 150f;
    public float MinTalkFrameVolume = 0.01f;
    public int SoundTouchMaxSamplesToProcess = 1024;
    public float ListenSilenceStartFactor = 1.3f;
    public float ListenSilenceEndFactor = 1.2f;
    public int ListenAverageFramesCount = 5;
    public float ListenStartRecordingFactor = 0.001525925f;
    public int ListenStopMaxSilenceChunks = 5;
    public float ListenNormalizationFactor = 0.7629628f;
    public float ListenNoiseLimit = 0.02746666f;
    public float ListenSilenceEnviromentAbsDeltaStart = 0.06103702f;
    public float ListenSilenceEnviromentAbsDeltaEnd = 0.03051851f;
    public int TalkFrameBins = 32;
    public int MicrophoneChannels = 1;
    public float MicrophoneRecordingStartDelay = 0.1f;
    protected const float maxFloatInt = 32767f;
    public float SoundTouchTempoValue;
    public float SoundTouchPitch;
    public float SoundTouchRate;
    public string MicrophoneDeviceName;
    public float ListenEndCutoff;
    public float ListenEndFadeout;

    public virtual float SoundTouchTempo
    {
      get
      {
        return this.SoundTouchTempoValue;
      }
    }

    public virtual bool ListeningEnabled
    {
      get
      {
        if (Microphone.devices != null)
          return Microphone.devices.Length > 0;
        return false;
      }
    }

    public virtual int SampleRate
    {
      get
      {
			#if UNITY_ANDROID
			int minFreq, maxFreq;
			Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);
			minFreq = Mathf.Max(minFreq, 100);
			maxFreq = Mathf.Max(maxFreq, 100);
			return Mathf.Clamp(44100, minFreq, maxFreq);
			#else
			return Mathf.Clamp(frequency, 100, 44100);
			#endif
      }
    }
  }
}
